<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from cpbootstrap.com/foxlabel/lightdark_layout/ by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 16 Jun 2017 10:53:25 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MEB</title>
    <!-- Css Files -->
    <link href="<?php echo base_url();?>assets/office/css/root.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/office/css/angular-material.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/office/js/wallet.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
    <style>
	 .buy-{
	 box-shadow: 0 3px 5px -1px rgba(0,0,0,.2), 0 6px 10px 0 rgba(0,0,0,.14), 0 1px 18px 0 rgba(0,0,0,.12);
    width: 120px;
    height: 60px;
    display: inline-block;
    line-height: 60px;
    text-align: center;
    font-size: 30px;
    background: none repeat scroll 0% 0% #EEE;
    border-radius: 100%;
    float: left;
    margin-right: 10px;
	 }
</style>
</head>
<body>
    <!-- Start Page Loading -->
    <div class="loading"><img src="<?php echo base_url();?>assets/office/img/loading.gif" alt="loading-img"></div>
    <!-- End Page Loading --> 
    <!-- Start Top -->
    <div id="top" class="clearfix"> 
        <!-- Start App Logo -->
        <div class="applogo"> <a href="<?php echo base_url();?>index.php/office" class="logo">MWEB</a> </div>
        <!-- End App Logo --> 
        <!-- Start Sidebar Show Hide Button --> 
        <a href="#" class="sidebar-open-button"><i class="fa fa-bars"></i></a> <a href="#" class="sidebar-open-button-mobile"><i class="fa fa-bars"></i></a> 
        <!-- End Sidebar Show Hide Button --> 
        <!-- Start Searchbox -->
        <form class="searchform">
			<a href="#" >BTC/ZAR 37,881</a>
        </form>
        <!-- End Searchbox --> 
        <!-- Start Sidepanel Show-Hide Button --> 
        <!--<a href="#sidepanel" class="sidepanel-open-button"><i class="fa fa-outdent"></i></a> -->
        <!-- End Sidepanel Show-Hide Button --> 
        <!-- Start Top Right -->
        <ul class="top-right">
           <!-- <li class="dropdown dropdown-notification nav-item link"><a href="#" data-toggle="dropdown" class="nav-link nav-link-label count-info"><span class="label label-warning">6</span><i class="fa fa-envelope" aria-hidden="true"></i></a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right top-width">
                    <li class="dropdown-menu-header">
                        <h6 class="dropdown-header"><span>Notifications</span><span class="pull-right label label-danger">6 New</span></h6>
                    </li>
                    <li class="list-group"><a href="#" class="list-group-item">
                        <div class="media">
                            <div class="media-body">
                                <h6 class="media-heading color10"><i class="fa fa-shopping-cart"></i> You have new order!</h6>
                                <p class="notification-text font-small-3 text-muted">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                <small>
                                    <time datetime="2017-02-14 20:00" class="media-meta text-muted">30 minutes ago</time>
                                </small> </div>
                            </div>
                        </a><a href="#" class="list-group-item">
                        <div class="media">
                            <div class="media-body">
                                <h6 class="media-heading darken-1 color7"><i class="fa fa-desktop"></i> 99% Server load</h6>
                                <p class="notification-text font-small-3 text-muted">Aliquam tincidunt mauris eu risus.</p>
                                <small>
                                    <time datetime="2017-02-14 20:00" class="media-meta text-muted">30 minutes ago</time>
                                </small> </div>
                            </div>
                        </a><a href="#" class="list-group-item">
                        <div class="media">
                            <div class="media-body">
                                <h6 class="media-heading color9"><i class="fa fa-server"></i> Warning notifixation</h6>
                                <p class="notification-text font-small-3 text-muted">Vestibulum auctor dapibus neque.</p>
                                <small>
                                    <time datetime="2017-02-14 20:00" class="media-meta text-muted">30 minutes ago</time>
                                </small> </div>
                            </div>
                        </a><a href="#" class="list-group-item">
                        <div class="media">
                            <div class="media-body">
                                <h6 class="media-heading color11"><i class="fa fa-check"></i> Complete the task</h6>
                                <small>
                                    <time datetime="2017-02-14 20:00" class="media-meta text-muted">30 minutes ago</time>
                                </small> </div>
                            </div>
                        </a><a href="#" class="list-group-item">
                        <div class="media">
                            <div class="media-body">
                                <h6 class="media-heading"><i class="fa fa-bar-chart"></i> Generate monthly report</h6>
                                <small>
                                    <time datetime="2017-02-14 20:00" class="media-meta text-muted">30 minutes ago</time>
                                </small> </div>
                            </div>
                        </a></li>
                        <li class="dropdown-menu-footer"><a href="#" class="dropdown-item text-muted text-center">Read all notifications</a></li>
                    </ul>
                </li>-->
                <li class="dropdown link"> <a href="#" data-toggle="dropdown" class="dropdown-toggle profilebox"><img src="<?php echo base_url();?>assets/office/img/profileimg.png" alt="img"><b>John Doe</b><span class="caret"></span></a>
                    <ul class="dropdown-menu dropdown-menu-list dropdown-menu-right">
                        <li role="presentation" class="dropdown-header">Profile</li>
                        <li><a href="<?php echo base_url();?>index.php/office/profile"><i class="fa falist fa-wrench"></i>Settings</a></li>
                        <li><a href="<?php echo base_url();?>index.php/office"><i class="fa falist fa-power-off"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
            <!-- End Top Right --> 
        </div>
        <!-- End Top --> 
        <!-- Start Sidabar -->
        <div class="sidebar clearfix">
            <ul class="sidebar-panel nav metismenu" id="side-menu" data-plugin="metismenu">
                <li><a href="<?php echo base_url();?>index.php/office/board"><span class="icon color5"><i class="fa fa-home" aria-hidden="true"></i></span>Dashboard</a>
                   
                </li>
                
                <li><a href="<?php echo base_url();?>index.php/office/accounts"><span class="icon color5"><i class="fa fa-home" aria-hidden="true"></i></span>Accounts</a>
                   
                </li>
                <li><a href="<?php echo base_url();?>index.php/office/market_order"><span class="icon color5"><i class="fa fa-home" aria-hidden="true"></i></span>Exchange</a>
                   
                </li>
                <li><a href="#"><span class="icon color5"><i class="fa fa-home" aria-hidden="true"></i></span><span class="nav-title">Affiliate</span> <i class="fa fa-sort-desc caret"></i></a>
                    <ul>
                        <li><a href="<?php echo base_url();?>index.php/office/aff_board"><i class="fa fa-angle-right" aria-hidden="true"></i> Dashboard</a></li>
                        <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Team
                        </a></li>
                        <li><a href="index-4.html"><i class="fa fa-angle-right" aria-hidden="true"></i> Earnings</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- End Sidabar --> 
        <!-- Start Content -->
        <div class="page-content"> 
            <!-- Start Container -->
            <?php $this->load->view($content);?>
<!-- End Container --> 
<!-- Start Footer -->

<!-- End Footer --> 
</div>

<!-- End Content --> 
<!-- Start Sidepanel -->
<div role="tabpanel" class="sidepanel"> 
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item"><a class="nav-link active" href="#today" aria-controls="today" role="tab" data-toggle="tab">TODAY</a></li>
        <li class="nav-item"><a class="nav-link" href="#tasks" aria-controls="tasks" role="tab" data-toggle="tab">TASKS</a></li>
        <li class="nav-item"><a class="nav-link" href="#chat" aria-controls="chat" role="tab" data-toggle="tab">CHAT</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content"> 
        <!-- Start Today -->
        <div role="tabpanel" class="tab-pane active" id="today">
            <div class="sidepanel-m-title"> Today <span class="left-icon"><a href="#"><i class="fa fa-refresh"></i></a></span> <span class="right-icon"><a href="#"><i class="fa fa-file-o"></i></a></span> </div>
            <div class="gn-title">NEW</div>
            <ul class="list-w-title">
                <li> <a href="#"> <span class="label label-danger">ORDER</span> <span class="date">9 hours ago</span>
                    <h4>New Jacket 2.0</h4>
                    Etiam auctor porta augue sit amet facilisis. Sed libero nisi, scelerisque. </a> </li>
                    <li> <a href="#"> <span class="label label-success">COMMENT</span> <span class="date">14 hours ago</span>
                        <h4>Bill Jackson</h4>
                        Etiam auctor porta augue sit amet facilisis. Sed libero nisi, scelerisque. </a> </li>
                        <li> <a href="#"> <span class="label label-info">MEETING</span> <span class="date">at 2:30 PM</span>
                            <h4>Developer Team</h4>
                            Etiam auctor porta augue sit amet facilisis. Sed libero nisi, scelerisque. </a> </li>
                            <li> <a href="#"> <span class="label label-warning">EVENT</span> <span class="date">3 days left</span>
                                <h4>Birthday Party</h4>
                                Etiam auctor porta augue sit amet facilisis. Sed libero nisi, scelerisque. </a> </li>
                            </ul>
                        </div>
                        <!-- End Today --> 
                        <!-- Start Tasks -->
                        <div role="tabpanel" class="tab-pane" id="tasks">
                            <div class="sidepanel-m-title"> To-do List <span class="left-icon"><a href="#"><i class="fa fa-pencil"></i></a></span> <span class="right-icon"><a href="#"><i class="fa fa-trash"></i></a></span> </div>
                            <div class="gn-title">TODAY</div>
                            <div class="form-group has-success">
                              <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Add new products</span>
                            </label>
                        </div>
                        <div class="form-group has-warning">
                          <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">May 14, 8:20 pm Meeting with Team</span>
                        </label>
                    </div>
                    <div class="form-group has-danger mb-0">
                      <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Design Facebook page</span>
                    </label>
                </div>
                <div class="form-group has-info mb-0">
                  <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Send Invoice to customers</span>
                </label>
            </div>
            <div class="form-group has-success mb-0">
              <label class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Meeting with developer team</span>
            </label>
        </div>
        <div class="gn-title">TOMORROW</div>
        <ul class="todo-list">
            <li class="checkbox checkbox-warning">
                <input id="checkboxside6" type="checkbox">
                <label for="checkboxside6">Redesign our company blog</label>
            </li>
            <li class="checkbox checkbox-success">
                <input id="checkboxside7" type="checkbox">
                <label for="checkboxside7">Finish client work</label>
            </li>
            <li class="checkbox checkbox-info">
                <input id="checkboxside8" type="checkbox">
                <label for="checkboxside8">Call Johnny from Developer Team</label>
            </li>
        </ul>
    </div>
    <!-- End Tasks --> 
    <!-- Start Chat -->
    <div role="tabpanel" class="tab-pane" id="chat">
        <div class="sidepanel-m-title"> Friend List <span class="left-icon"><a href="#"><i class="fa fa-pencil"></i></a></span> <span class="right-icon"><a href="#"><i class="fa fa-trash"></i></a></span> </div>
        <div class="gn-title">ONLINE MEMBERS (3)</div>
        <ul class="group">
            <li class="member"><a href="#"><img src="img/profileimg.png" alt="img"><b>Allice Mingham</b>Los Angeles</a><span class="status online"></span></li>
            <li class="member"><a href="#"><img src="img/profileimg3.png" alt="img"><b>James Throwing</b>Las Vegas</a><span class="status busy"></span></li>
            <li class="member"><a href="#"><img src="img/profileimg2.png" alt="img"><b>Fred Stonefield</b>New York</a><span class="status away"></span></li>
            <li class="member"><a href="#"><img src="img/profileimg4.png" alt="img"><b>Chris M. Johnson</b>California</a><span class="status online"></span></li>
        </ul>
        <div class="gn-title">OFFLINE MEMBERS (8)</div>
        <ul class="group">
            <li class="member"><a href="#"><img src="img/profileimg.png" alt="img"><b>Allice Mingham</b>Los Angeles</a><span class="status offline"></span></li>
            <li class="member"><a href="#"><img src="img/profileimg3.png" alt="img"><b>James Throwing</b>Las Vegas</a><span class="status offline"></span></li>
        </ul>
        <form class="search">
            <input type="text" class="form-control" placeholder="Search a Friend...">
        </form>
    </div>
    <!-- End Chat --> 
</div>
</div>-->
<!-- End Sidepanel --> 
<!-- jQuery Library --> 
<script type="text/javascript" src="<?php echo base_url();?>assets/office/js/jquery.min.js"></script> 
<!-- Bootstrap Core JavaScript File --> 
<script src="<?php echo base_url();?>assets/office/js/bootstrap/tether.min.js"></script>
<script src="<?php echo base_url();?>assets/office/js/bootstrap/bootstrap.min.js"></script> 
<!-- Plugin.js - Some Specific JS codes for Plugin Settings --> 
<script type="text/javascript" src="<?php echo base_url();?>assets/office/js/plugins.js"></script>
<!-- MetisMenu --> 
<script type="text/javascript" src="<?php echo base_url();?>assets/office/js/metismenu/metisMenu.min.js"></script>
<!-- Flot Chart --> 
<script type="text/javascript" src="<?php echo base_url();?>assets/office/js/flot-chart/flot-chart.js"></script><!-- main file --> 
<script type="text/javascript" src="<?php echo base_url();?>assets/office/js/flot-chart/flot-chart-time.js"></script><!-- time.js --> 
<script type="text/javascript" src="<?php echo base_url();?>assets/office/js/flot-chart/flot-chart-stack.js"></script><!-- stack.js --> 
<script type="text/javascript" src="<?php echo base_url();?>assets/office/js/flot-chart/flot-chart-pie.js"></script><!-- pie.js --> 
<script type="text/javascript" src="<?php echo base_url();?>assets/office/js/flot-chart/flot-chart-plugin.js"></script><!-- demo codes --> 
<!-- Chartist --> 
<script type="text/javascript" src="<?php echo base_url();?>assets/office/js/chartist/chartist.js"></script><!-- main file --> 
<script type="text/javascript" src="<?php echo base_url();?>assets/office/js/chartist/chartist-plugin.js"></script><!-- demo codes --> 


<!-- Counterup -->
<script type="text/javascript" src="<?php echo base_url();?>assets/office/js/counterup/jquery.waypoints.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/office/js/counterup/jquery.counterup.min.js"></script>
</body>

</html>