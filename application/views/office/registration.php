<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from cpbootstrap.com/foxlabel/lightdark_layout/register.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 16 Jun 2017 11:10:58 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MEB | Register</title>
    <!-- Css Files -->
    <link href="<?php echo base_url();?>assets/office/css/root.css" rel="stylesheet">
    <style type="text/css">
        body {
            background: #F5F5F5;
        }
    </style>
</head>
<body>
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
               
            </div>
            
            <form class="m-t contact-box" action="<?php echo base_url();?>index.php/office/confirmation_send">
               
               <h3>Register to MEB</h3>
            <p>Create account to see it in action.</p>
               
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Firstname" required="">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Surname" required="">
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email" required="">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Blockchain Bitcoin Address" required="">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" required="">
                </div>
                <div class="form-group">
                        <label>
                            <i></i>By signing up you agree to our
							<a>Terms of Use</a> and <a>Privacy Policy</a>. </label>
                       
                    </div>
                    <button type="submit" class="btn btn-success block full-width m-b">Register</button>
                    <p class="text-muted text-center"><small>Already have an account?</small></p>
                    <a class="btn btn-sm btn-white btn-block" href="<?php echo base_url();?>index.php/office">Login</a>
                </form>
                <p class="m-t"> <small> &copy; 2017</small> </p>
            </div>
        </div>
    </body>
    
</html>