 
 <style>
.balances-header {
  background: #00bcd4;
  color: #ffffff;
  padding: 22px 0 52px 0;
  cursor: pointer;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
}
.balances-header .container {
  height: 80px;
}
.balances-header img {
  display: block;
  margin-top: 25px;
}
.balances-header span {
  margin: 0;
  display: block;
  text-align: center;
}
.balances-header span.md-subhead {
  color: #ffffff;
  opacity: 0.5;
}
				 
				 .qab {
  margin-top: -50px;
  margin-bottom: 8px;
}
.qab .md-whiteframe-4dp {
  border-radius: 2px;
  background: #ffffff;
}
.qab md-icon {
  color: #212121;
}
.qab a.md-button:not(.md-primary) {
  color: #757575 !important;
  text-transform: none;
  font-weight: normal;
  padding-top: 10px;
  margin: 0;
	float: 
}
</style>
            <!--<link href="<?php echo base_url();?>assets/office/chart/radsstyle.css" rel="stylesheet">
            <link href="<?php echo base_url();?>assets/office/chart/indicators.css" rel="stylesheet">
            -->
            
            
             <div layout="column" layout-align="center center" layout-margin="" class="md-whiteframe-1dp balances-header layout-margin  layout-column"  role="button" tabindex="0">
             
				 <div class="container">
					<div  aria-hidden="false" class="" style="">
						<div class="row">
							<div class="text-left col-md-3" width="50%">
								<span class="md-subhead">BTC Account</span> <span class="md-headline ng-binding">BTC 0.00</span> 
								
							</div>
							
							<div class="text-right pull-right col-md-3" width="50%">
								<span class="md-subhead ">ZAR Account</span> <span class="md-headline ng-binding">ZAR 0.00</span> 
								
							</div>
						</div>
					</div>
					
				
				</div>
           	
           	</div>
         
             
             
             
             <div layout="row" layout-align="center center" class="qab layout-align-center-center layout-row" ng-show="!vm.loading" aria-hidden="false" style="">
             	<div class="md-whiteframe-4dp layout-margin layout-align-center-center layout-row" layout-margin="" layout="row" layout-align="center center">
             		<a class="md-button ng-scope md-ink-ripple layout-column" layout="column" href="#" aria-hidden="false" style="" data-toggle="modal" data-target="#send_btc"><md-icon class="ng-scope material-icons" aria-label="send" role="img">send</md-icon><span class="ng-binding ng-scope">Fund</span></a>
					
            		<a class="md-button ng-scope md-ink-ripple layout-column" layout="column" href="#" aria-hidden="false" style="" data-toggle="modal" data-target="#buy-sell">
            		<md-icon src="https://d32exi8v9av3ux.cloudfront.net/web/88cbf2e/img/bitcoin.svg" class="ng-scope" aria-hidden="true"></md-icon><span class="ng-binding ng-scope">Buy</span></a>
            		
            		<a class="md-button ng-scope md-ink-ripple layout-column" layout="column" href="#" aria-hidden="false" style=""><md-icon class="ng-scope material-icons" aria-label="account_balance_wallet" role="img">account_balance_wallet</md-icon><span class="ng-binding ng-scope">Withdraw</span></a>
					
            		<a class="md-button md-ink-ripple layout-column" href="#" layout="column" aria-hidden="false" style="" data-toggle="modal" data-target="#buy-sell"><md-icon class="ng-scope material-icons" aria-label="account_balance_wallet" role="img">account_balance_wallet</md-icon><span translate="" class="ng-scope"><span class="ng-scope">Transfer</span></span></a>
            	</div>
            </div>
             

             
             
 <!-- Modal -->
<div class="modal fade" id="send_btc" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Send BTC</h4>
			</div>
			<div class="modal-body">
			<div class="form-group">
				<label for="input1" class="form-label">Send to <span style="color:red">*</span></label>
				<input type="text" class="form-control form-control-line" placeholder="Mobile numbe,Bitcoin or email address" id="input1">
			</div>
		   <div class="form-group">
				<label for="input1" class="form-label">Currency <span style="color:red">*</span></label>
				
				<select class="form-control form-control-line" >
					<option value="BTC">BTC</option>
			   </select>
			</div>
		   <div class="form-group">
				<label for="input1" class="form-label">Amount <span style="color:red">*</span></label>
				<input type="text" class="form-control form-control-line" placeholder="Amount" id="input1">
			</div>
			<div class="form-group">
				<label for="input1" class="form-label">Reference <span style="color:red">*</span></label>
				<input type="text" class="form-control form-control-line" placeholder="Reference" id="input1">
			</div>
		   </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-default">Send</button>
			</div>
		</div>
	</div>
</div>


<!-- Buy or sell Modal-->
 <!-- Modal -->
<div class="modal fade" id="buy-sell" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Instant buy/sell</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div role="tabpanel"> 
					<!-- Nav tabs -->
					<ul class="nav nav-tabs nav-justified" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#buy" role="tab">Buy</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#sell" role="tab">Sell</a>
						</li>
						
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="buy" role="tabpanel" style="text-align: center;">
							<img src="<?php echo base_url();?>assets/office/img/empty-state.png"/><br>
							You need to deposit money into your account before you can buy BTC.<br><br>
						<button type="button" data-toggle="modal" data-target="#deposit" class="btn btn-default">Deposit money</button>
						</div>
						<div class="tab-pane" id="sell" role="tabpanel">
							<div class="form-group">
								<label for="input1" class="form-label">Amount to sell to bitcoin <span style="color:red">*</span></label>
							</div>
						   <div class="form-group form-inline">
								<select class="form-control-line" >
									<option value="BTC">BTC</option>
							   </select>
							   <input type="text" value="0" class="form-control form-control-line" placeholder="Amount" id="input1">
							   
							</div>
						   <div class="form-group">
								<label for="input1" class="form-label">You have: <span style="color:red">*</span></label>
								<input type="text" class="form-control form-control-line" placeholder="Amount" value="BTC 0.00000000" disabled id="input1">
							</div>
							<div class="form-group">
								<label for="input1" class="form-label">Estimated rate per Bitcoin: <span style="color:red">*</span></label>
								<input type="text" class="form-control form-control-line" placeholder="Reference" value="BTC/ZAR 37,366" disabled id="input1">
							</div>
						<button type="button" class="btn btn-block">Sell</button>
						</div>
						
					</div>
				</div>
                            
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				
			</div>
		</div>
	</div>
</div>
<!-- End Moda Code -->


 <!-- Modal -->
<div class="modal fade" id="deposit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Which wallet would you like to add money to?</h4>
			</div><a href="#" data-toggle="modal" data-target="#eft" onmouseover="this.style.color='grey'">
			<div class="modal-body">
				<img src="<?php echo base_url();?>assets/office/img/ZAR.png" width="40px" height="40px" style="border-radius:50%"/> ZAR Account
		   </div></a>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Back</button>
				
			</div>
		</div>
	</div>
</div>

 <!-- Modal -->
<div class="modal fade" id="eft" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">How would you like to add money your Luno wallet?</h4>
			</div><a href="#" data-toggle="modal" data-target="#eft_banks">
			<div class="modal-body">
				<img src="<?php echo base_url();?>assets/office/img/bank.svg"  style="border-radius:50%"/> EFT
		   </div></a>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Back</button>
				
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="eft_banks" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Which service would you like to use to add money to your Luno wallet?</h4>
			</div>
				
				<table class="table">
					<tbody>
						
						<tr data-toggle="modal" data-target="#amount_to_deposit" style="cursor: pointer;">
							<td>
								<img src="<?php echo base_url();?>assets/office/img/absa.png"  style="border-radius:50%" width="40px" height="40px"/> Absa
							</td>
						</tr>
						<tr data-toggle="modal" data-target="#amount_to_deposit" style="cursor: pointer;">
							<td>
								<img src="<?php echo base_url();?>assets/office/img/fnb.png"  style="border-radius:50%" width="40px" height="40px"/> FND
							</td>
						</tr>
					</tbody>
				</table>
		   
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Back</button>
				
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="amount_to_deposit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">How much would you like to deposit?</h4>
			</div>
			<div class="modal-body">
				
			<div class="form-group">
				<label for="input1" class="form-label">ZAR Amount <span style="color:red">*</span></label>
				<input type="text" class="form-control form-control-line" id="amount_zar" placeholder="Reference" onchange="set_value_(this);">
			</div>
		   </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Back</button>
				<button type="button" class="btn btn-default" data-toggle="modal" data-target="#payment_info" style="cursor: pointer;">Next</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="payment_info" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">How much would you like to deposit?</h4>
			</div>
			<div class="modal-body">
	   
				<h4 class="text-c">Make a deposit</h4>

				<p class="text-c">Make a deposit using this reference:</p>

				<h3 class="text-c">BX62U5W63</h3>
				<p class="text-c">Sign in to your bank, create a recipient and make a deposit using this information</p>

				<table class="table">
					<tbody>
						<tr>
							<td class="text-l">Name</td>
							<td class="text-r">MWEB</td>
						</tr>
						<tr>
							<td class="text-l">Bank</td>
							<td class="text-r">Absa</td>
						</tr>
						<tr>
							<td class="text-l">Account Number</td>
							<td class="text-r">62412967484</td>
						</tr>
						<tr>
							<td class="text-l">Branch</td>
							<td class="text-r">343443</td>
						</tr>
						<tr>
							<td class="text-l">Type</td>
							<td class="text-r">Current</td>
						</tr>
						<tr>
							<td class="text-l">Reference</td>
							<td class="text-r">BX62U5W63</td>
						</tr>
						<tr>
							<td class="text-l">Amount</td>
							<td class="text-r">ZAR <span id="amount_buying">11.00</span></td>
						</tr>
					</tbody>
				
				</table>

				<ul>
					<li>Use payment reference BX62U5W63</li>
					<li>EFT only (no cash deposit)</li>
					<li>Pay from your own bank account</li>
				</ul>
		   </div>
			<div class="modal-footer">
				
				<button type="button" class="btn btn-default btn-block" data-toggle="modal" data-target="#payment_info">I HAVE SENT THE PAYMENT</button>
				<button type="button" class="btn btn-white btn-block" data-dismiss="modal">Back</button>
			</div>
		</div>
	</div>
</div>
<!--
 <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
 <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/office/chart/highstock.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/office/chart/exporting.js"></script>
 <script type="text/javascript" src="<?php echo base_url();?>assets/office/chart/btc_chart_head.js"></script>
            <script type="text/javascript" src="<?php echo base_url();?>assets/office/chart/highstock.js"></script>
            <script type="text/javascript" src="<?php echo base_url();?>assets/office/chart/exporting.js"></script>
           
-->
<script>
function eft(){
	
}
	
function serviceProvider(){
	
}

function set_value_(money=''){

	//localStorage.setItem("amount", money.value);
	$('#amount_buying').html(money.value);
	
}
</script>


<script>
	<!--
	function radsBuyTotBTC(){
	
		var radsBuyBTC = Number(document.buybtc.amount_coins.value);
		var radsBuyPriceBTC = Number(document.buybtc.coin_price.value);
		var radsTBpriceBTC = radsBuyBTC * radsBuyPriceBTC;
		
		document.buybtc.buybtcfee.value= (radsBuyBTC/100*0.8).toFixed(8);
		document.buybtc.buybtctotal.value= radsTBpriceBTC;
		return true;
	}
function radsSureBuyBTC(){

		var radsBuyBTC = Number(document.buybtc.amount_coins.value);
		var radsBuyPriceBTC = Number(document.buybtc.coin_price.value);
		var radsTBpriceBTC = radsBuyBTC * radsBuyPriceBTC;
		
		var radsTBfeeBTC = radsBuyBTC /100*0.8;
		var rads8decFBbtc = radsTBfeeBTC.toFixed(8);
		var rads8decBbtc = radsTBpriceBTC.toFixed(8);/**/
				
		var messageBTC = "Confirm buy order of " + radsBuyBTC + " BTC at R " + radsBuyPriceBTC + " each. \nFee BTC " + rads8decFBbtc + "  \nTotal buy price R " + rads8decBbtc ;
		//var messageB = "Confirm buy order of ";
		if (confirm(messageBTC))
			return true;
			else
			return false;
	}

	
	
	

	function radsSellTotBTC(){
	
		var radsSellBTC = Number(document.sellbtc.amount_coins.value);
		var radsSellPriceBTC = Number(document.sellbtc.coin_price.value);
		var radsTSpriceBTC = radsSellBTC * radsSellPriceBTC;
		document.sellbtc.sellbtcfee.value= (radsTSpriceBTC/100*0.8).toFixed(8);
		document.sellbtc.sellbtctotal.value= radsTSpriceBTC;
		return true;
	}
	function radsSureSellBTC(){

		var radsSellBTC = Number(document.sellbtc.amount_coins.value);
		var radsSellPriceBTC = Number(document.sellbtc.coin_price.value);
		var radsTSpriceBTC = radsSellBTC * radsSellPriceBTC;
		var radsTSfeeBTC = radsSellBTC * radsSellPriceBTC/100*0.8;
		var rads8decFSbtc = radsTSfeeBTC.toFixed(8);
		var rads8decSbtc = radsTSpriceBTC.toFixed(8);
				
		var messageS = "Confirm sell order of " + radsSellBTC + " BTC at R " + radsSellPriceBTC + " each. \nFee R " + rads8decFSbtc + " \nTotal sell price R " + rads8decSbtc ;
		if (confirm(messageS))
			return true;
			else
			return false;
	}

	-->
</script>
<noscript>
This site requires JavaScript!
</noscript>