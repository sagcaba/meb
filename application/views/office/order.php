<div class="row page-titles">
	<div class="col-md-6 col-8 align-self-center">
		<h3 class="text-themecolor m-b-0 m-t-0">Exchange</h3>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
			<li class="breadcrumb-item active">Exchange</li>
		</ol>
	</div>
	<div class="col-md-6 col-4 align-self-center">
	
	</div>
</div>

<div class="row">
	<!-- column -->
	<div class="col-lg-6">
	
	<div class="panel panel-default">
		<div class="panel-title" style="background-color: #00bcd4;color:white !important;"> <h1 style="color:white !important;"> Buy BTC</h1> </div>
			<div class="panel-body">
				
				
			</div>
		</div>
		
		
		
	</div>
	
	<div class="col-lg-6">
	
	<div class="panel panel-default">
		<div class="panel-title" style="background-color: #00bcd4;color:white !important;"> <h1 style="color:white !important;"> Sell BTC</h1> </div>
			<div class="panel-body">
				
			</div>
			
			
		</div>
		
		
	</div>
</div><!--end row-->

<div class="row">
	<!-- column -->
	<div class="col-lg-6">
	
	<div class="panel panel-default">
		<div class="panel-title" style="background-color: #00bcd4;color:white !important;"> <h1 style="color:white !important;"> Current SELL offers</h1> </div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Price(USD)</th>
								<th>Amount(BTC)</th>
								<th>Value(BTC)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>332.23</td>
								<td>2323.122</td>
								<td>4554.43</td>
							</tr>
							<tr>
								<td>2</td>
								<td>12.34</td>
								<td>12.2323</td>
								<td>7856.32</td>
							</tr>
						</tbody>
					</table>
				</div>
				
			</div>
		</div>
		
		
		
	</div>
	
	<div class="col-lg-6">
	
	<div class="panel panel-default">
		<div class="panel-title" style="background-color: #00bcd4;color:white !important;"> <h1 style="color:white !important;"> Current Buy offers</h1> </div>
			<div class="panel-body">
					<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Price(USD)</th>
								<th>Amount(BTC)</th>
								<th>Value(BTC)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>332.23</td>
								<td>2323.122</td>
								<td>4554.43</td>
							</tr>
							<tr>
								<td>2</td>
								<td>12.34</td>
								<td>12.2323</td>
								<td>7856.32</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			
			
		</div>
		
		
	</div>
</div><!--end row-->

<div class="row">
	<!-- column -->
	<div class="col-lg-12">
	
	<div class="panel panel-default">
		<div class="panel-title" style="background-color: #00bcd4;color:white !important;"> <h1 style="color:white !important;"> Trade History</h1> </div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Price(USD)</th>
								<th>Amount(BTC)</th>
								<th>Value(BTC)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>332.23</td>
								<td>2323.122</td>
								<td>4554.43</td>
							</tr>
							<tr>
								<td>2</td>
								<td>12.34</td>
								<td>12.2323</td>
								<td>7856.32</td>
							</tr>
						</tbody>
					</table>
				</div>
				
			</div>
		</div>
		
		
		
	</div>
	
	
</div><!--end row-->
<!--Buy Bitcoin-->
                 <div class="modal fade" id="buy_bitcoin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title" id="">Buy Bitcoin</h4>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							</div>
							<div class="modal-body">
								
								
								<ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item"> 
                                    	<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home5" role="tab" aria-controls="home5" aria-expanded="true"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Buy</span></a>
                                    </li>
                                    <li class="nav-item disabled" > 
                                    	<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile5" role="tab" aria-controls="profile"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down" disabled>Payment</span></a>
                                    </li>
                                   
                                </ul>
                                
                                <div class="tab-content tabcontent-border p-20" id="myTabContent">
                                    <div role="tabpanel" class="tab-pane fade show active" id="buy" aria-labelledby="home-tab">
                                       <form class="form-material">
											<div class="form-group">
												<label for="message-text" class="control-label">Currency<span style="color:red;">*</span>:</label>
												<select class="form-control" id="-text1">
													<option selected>BTC</option>
												</select>
											</div>
											<div class="form-group">
												<label for="message-text" class="control-label">Amount<span style="color:red;">*</span>:</label>
												<input type="text" class="form-control" id="-name1">
											</div>
											<div class="form-group">
												<label for="message-text" class="control-label">Reference:</label>
												<textarea class="form-control" id="-text1"></textarea>
											</div>
										</form>
                                    </div>
                                    <div class="tab-pane fade" id="profile5" role="tabpanel" aria-labelledby="profile-tab">
                                      <form class="form-material">
											<div class="form-group">
												<label for="message-text" class="control-label">Payment Method<span style="color:red;">*</span>:</label>
												<select class="form-control" id="-text1">
													<option selected>BTC</option>
													<option >ZAR</option>
												</select>
											</div>
										</form>
                                    </div>
                                   
                                </div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								
							</div>
						</div>
					</div>
				</div>
				<!-- /.modal -->

