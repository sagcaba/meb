<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from cpbootstrap.com/foxlabel/lightdark_layout/register.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 16 Jun 2017 11:10:58 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MEB | Register</title>
    <!-- Css Files -->
    <link href="<?php echo base_url();?>assets/office/css/root.css" rel="stylesheet">
    <style type="text/css">
        body {
            background: #F5F5F5;
        }
    </style>
</head>
<body>
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
               
            </div>
           
            <form class="m-t contact-box" action="#">
                <h3>Confirm Email</h3>
                <img src="<?php echo base_url();?>assets/office/img/confirm_email.png" class="img-circle"/>
				<p class="text-center">We have sent an confirmation email to <strong>m.n.moroka@gmail.com</strong></p>
               
               
                    <a class="btn btn-lg  btn-default btn-block" href="<?php echo base_url();?>index.php/office">CONFIRMED</a>
                    
                    <a class="btn btn-lg  btn-primary btn-block" href="#">RESEND</a>
                </form>
                <p class="m-t"> <small> &copy; 2017</small> </p>
            </div>
        </div>
    </body>
    
</html>