<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MWEB | Forgot Password</title>
    <!-- Css Files -->
    <link href="<?php echo base_url();?>assets/office/css/root.css" rel="stylesheet">
    <style type="text/css">
        body {
            background: #F5F5F5;
        }
    </style>
</head>
<body>
    <div class="passwordBox animated fadeInDown">
        <div class="row">
            <div class="col-md-12">
                <div class="PasswordContent">
                    <h2 class="font-bold">Forgot password</h2>
                    <p> Enter your email address and password reset link will be emailed to you. </p>
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="m-t" action="#">
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email address" required="">
                                </div>
                                <button type="submit" class="btn btn-primary block full-width m-b">Send new password</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6"> </div>
            <div class="col-md-6 text-right"> <small>&copy; 2017</small> </div>
        </div>
    </div>
</body>
</html>