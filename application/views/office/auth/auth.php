<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from cpbootstrap.com/foxlabel/lightdark_layout/login.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 16 Jun 2017 11:10:58 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MEB - Login Page</title>
    <!-- Css Files -->
    <link href="<?php echo base_url();?>assets/office/css/root.css" rel="stylesheet">
    <style type="text/css">
        body {
            background: #F5F5F5;
        }
    </style>
</head>
<body>
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                
            </div>
            
            <form class="m-t contact-box" action="<?php echo base_url();?>index.php/office/board">
               <h3>Welcome to MEB</h3>
            
            	<p>Login</p>
               
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Username" required="">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                <br>
                <a href="<?php echo base_url();?>index.php/office/forgot"><small>Forgot password?</small></a>
                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="<?php echo base_url();?>index.php/office/register">Create an account</a>
            </form>
            <p class="m-t"> <small> &copy; 2017</small> </p>
        </div>
    </div>
</body>

<!-- Mirrored from cpbootstrap.com/foxlabel/lightdark_layout/login.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 16 Jun 2017 11:10:58 GMT -->
</html>