<!-- Start Container -->
            <div class="container-no-padding animated fadeInRight"> 
                <!-- Start Social Profile -->
                <div class="social-profile"> 
                    <!-- Start Top -->
                    <div class="social-top">
                        <div class="profile-left"> <img src="<?php echo base_url();?>assets/office/img/profileimg.png" alt="img" class="profile-img">
                            <h1 class="name">John Doe </h1>
                            <p class="profile-text">There can be no thought of finishing...</p>
                        </div>
                        <ul class="social-stats">
                            <li><b>17</b>followers</li>
                        </ul>
                    </div>
                    <!-- End Top --> 
                    <!-- Start Social Content -->
                    <div class="social-content clearfix"> 
                    	<div class="row">
							<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-title"> Personal Details </div>
								<div class="panel-body">
									<form>
										<div class="form-group">
											<label for="example3" class="form-label">Name</label>
											<input type="text" class="form-control form-control-line" id="example3">
										</div>
										<div class="form-group">
											<label for="example4" class="form-label">Surname</label>
											<input type="text" class="form-control form-control-line" id="example4">
										</div>
										<div class="form-group">
											<label for="example4" class="form-label">Blockchain Bitcpin Address</label>
											<input type="text" class="form-control form-control-line" id="example4">
										</div>
										<div class="form-group">
											<label for="example5"  class="form-label">Email</label>
											<input type="email" class="form-control form-control-line" id="example5">
										</div>
										
										<div class="form-group">
											<label for="example4" class="form-label">Phone Number</label>
											<input type="text" class="form-control form-control-line" id="example4">
										</div>
										<button type="submit" class="btn btn-default btn-block">Update</button>
									</form>
								</div>
							</div>
						</div>
	
							<div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-title"> Security </div>
                            <div class="panel-body">
                                <form>
                                    <div class="form-group">
                                        <label for="example3" class="form-label">Token</label>
                                        <input type="text" class="form-control form-control-line" id="example3">
                                    </div>
                                    <div class="form-group">
                                        <label for="example5"  class="form-label">Password</label>
                                        <input type="password" class="form-control form-control-line" id="example5">
                                    </div>
                                    <button type="submit" class="btn btn-default btn-block">Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
						</div>
                    </div>
				</div>
                            <!-- End Social Profile --> 
                        </div>