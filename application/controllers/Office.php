<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Office extends CI_Controller {


    public function __construct()
	{
		
		parent:: __construct();
		
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('session');
		
	}
	
	function index()
    {
        $this->load->view('office/auth/auth');
              
    }
	function aff_board()
    {
        $data = array('content'=>'office/dashboar_affiliate');
        $this->load->view('office/main' , $data);
              
    }
	function board()
    {
        $data = array('content'=>'office/home');
        $this->load->view('office/main' , $data);
              
    }
	function market_order()
    {
        $data = array('content'=>'office/order');
        $this->load->view('office/main' , $data);
              
    }
	function register()
    {
        $this->load->view('office/registration');
              
    }
	
	
	function confirmation_send()
    {
        $this->load->view('office/confirm_registation');
              
    }
	function forgot()
    {
        $data = array('content'=>'office/auth/forgot');
        $this->load->view('office/main' , $data);
              
    }
	function profile()
    {
        $data = array('content'=>'office/profile');
        $this->load->view('office/main' , $data);
              
    }
	
	function accounts()
    {
        $data = array('content'=>'office/accounts');
        $this->load->view('office/main' , $data);
              
    }
    
    
}
