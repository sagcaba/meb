<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {


    public function __construct()
	{
		
		parent:: __construct();
		
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('session');
		
	}
	function index()
    {
        $data = array('content'=>'site/home');
        $this->load->view('site/main' , $data);
              
    }
    
    
}
